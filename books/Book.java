package books;

/**
 * Класс книг
 *
 * @author Tropanova N.S.
 */

public class Book {

    private String author;
    private String name;
    private int year;
    private Genre genre;
    private String location;

    Book(String author, String name, int year, Genre genre, String location) {
        this.author = author;
        this.name = name;
        this.year = year;
        this.genre = genre;
        this.location = location;
    }

    String getAuthor() {
        return author;
    }

    String getName() {
        return name;
    }

    int getYear() {
        return year;
    }

    Genre getGenre() {
        return genre;
    }

    public String getLocation() {
        return location;
    }

    @Override
    public String toString() {
        return "Книга{" +
                "Автор='" + author + '\'' +
                ", Название книги='" + name + '\'' +
                ", Год издания=" + year +
                ", Жанр=" + genre +
                ", Путь к файлу='" + location + '\'' +
                '}';
    }
}
