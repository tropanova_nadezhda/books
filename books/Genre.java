package books;

/**
 * Класс жанров
 *
 * @author Tropanova N.S.
 */
public enum Genre {
    FANTASTIC, // Фантастика
    DETECTIVE, // Детектив
    CHILDISH, // детские
    NOVEL, // роман
    SCIENTIFIC // научные


}
