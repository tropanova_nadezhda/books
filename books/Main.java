package books;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Класс для реализации действий с книгами:
 * Сортировка списка книг по алфавиту по фамилиям авторов
 * Сортировка книг по году издания от новинок к более старым изданиям
 * Фильтрация списка книг по автору, по жанру
 * Поиск книг по названию
 *
 * @author Tropanova N.S.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        Book book1 = new Book("Достоевский", "Преступление и наказание", 1866, Genre.NOVEL, "D:\\Crime and punishment");
        Book book2 = new Book("Тургенев", "Отцы и дети", 1862, Genre.NOVEL, "D:\\Fathers and children");
        Book book3 = new Book("Дойл", "Приключения Шерлока Холмса", 1887, Genre.DETECTIVE, "D:\\Quiet don");
        Book book4 = new Book("Горький", "На дне", 1904, Genre.FANTASTIC, "D:\\At the bottom");
        Book book5 = new Book("Пушкин", "O рыбаке и рыбке", 1835, Genre.CHILDISH, "D:\\O fisherman and fish");

        ArrayList<Book> books = new ArrayList<>();
        books.add(book1);
        books.add(book2);
        books.add(book3);
        books.add(book4);
        books.add(book5);

        // поиск книг по названию
        System.out.println("Введите название книги для поиска: ");
        String name = scanner.nextLine();
        ArrayList<Book> nameBook = searchBook(books, name);
        if (nameBook.isEmpty()) {
            System.out.println("Таких книг нет в списке");
        }
        System.out.println(nameBook);
        System.out.println();

        // сортировка по алфавиту
        System.out.println("Сортировка книг по автору: ");
        sortAuthor(books);
        print(books);

        // сортировка по году издания
        System.out.println("Сортировка книг по году издания: ");
        sortYear(books);
        print(books);

        // фильтрация автора
        System.out.println("Введите автора книги для фильтрации: ");
        String author = scanner.nextLine();
        ArrayList<Book> bookAuthor = filtrationAuthor(books, author);
        if (bookAuthor.isEmpty()) {
            System.out.println("Таких книг нет в списке");
        }

        System.out.println(bookAuthor); // фильтрация по автору книги

        // фильтрация жанра
        Genre genre = Genre.SCIENTIFIC;
        System.out.println("Введите жанр книги для фильтрации:\n" +
                "1 - Фантастика\n" +
                "2 - Детектив\n" +
                "3 - Детские\n" +
                "4 - Роман\n ");

        int gender = scanner.nextInt();
        if (gender == 1) {
            genre = Genre.FANTASTIC;
        }
        if (gender == 2) {
            genre = Genre.DETECTIVE;
        }
        if (gender == 3) {
            genre = Genre.CHILDISH;
        }
        if (gender == 4) {
            genre = Genre.NOVEL;
        }
        ArrayList<Book> genreBook = filtrationGenre(books, genre);
        if (genreBook.isEmpty()) {
            System.out.println("Таких книг нет в списке");
        }
        System.out.println(genreBook);// фильтрация по жанру книги

    }


    /**
     * Метод сортирует массив объектов(@code books) в алфавитном порядке с помощью сортировки вставками
     *
     * @param books массив книг
     */
    private static void sortAuthor(ArrayList<Book> books) {
        for (int i = 1; i < books.size(); i++) {
            Book book = books.get(i);
            int temp = i;
            while (temp > 0 && books.get(temp - 1).getAuthor().compareTo(book.getAuthor()) >= 0) {
                books.set(temp, books.get(temp - 1));
                temp--;
            }
            books.set(temp, book);
        }
    }

    /**
     * Метод сортирует массив объектов(@code books) по году издания от новинок к более старым изданиям
     *
     * @param books массив книг
     */
    private static void sortYear(ArrayList<Book> books) {
        boolean flag = true;
        Book in; // вспомогательная переменная
        while (flag) {
            flag = false;
            int size = books.size();
            for (int j = 0; j < size - 1; j++) {
                if (books.get(j).getYear() < books.get(j + 1).getYear()) { // изменение элемента на 1

                    in = books.get(j); // меняем элементы местами
                    books.set(j, books.get(j + 1));
                    books.set(j + 1, in);
                    flag = true; // true означает что замена была проведена
                }
            }
        }
    }

    /**
     * Метод, выполяющий фильтрацию списка книг по автору
     * @param books массив книг
     * @param author введённый автор
     * @return список книг отфильтрованных по автору
     */
    private static ArrayList<Book> filtrationAuthor(ArrayList<Book> books, String author) {
        ArrayList<Book> authorsBook = new ArrayList<>();
        for (Book book : books) {
            if (author.equals(book.getAuthor())) {
                authorsBook.add(book);
            }
        }
        return authorsBook;
    }

    /**
     * Метод, выполяющий фильтрацию списка книг по жанру
     * @param books массив книг
     * @param genre введённый жанр
     * @return список книг отфильтрованных по жанру
     */
    private static ArrayList<Book> filtrationGenre(ArrayList<Book> books, Genre genre) {
        ArrayList<Book> genreBook = new ArrayList<>();
        for (Book book : books) {
            if (genre.equals(book.getGenre())) {
                genreBook.add(book);
            }
        }
        return genreBook;
    }

    private static ArrayList<Book> searchBook(ArrayList<Book> books, String name) {
        ArrayList<Book> nameBook = new ArrayList<>();
        for (Book book : books) {
            if (name.equals(book.getName())) {
                nameBook.add(book);
            }
        }
        return nameBook;
    }

    /**
     * Метод, реализующий вывод книг после сортировки
     *
     * @param books массив книг
     */
    private static void print(ArrayList<Book> books) {
        for (Book book : books) {
            System.out.println(book);
        }
        System.out.println();
    }

}
